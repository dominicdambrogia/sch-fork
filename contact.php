<?php

/*
  The about page for SHC

  Created on : Feb 17, 2016, 9:31:21 AM
  Author     : Dominic Dambrogia
  Contact    : domdambrogia@gmail.com
 */

?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/head.php"); ?>
        <title>Contact | Serenity Home Care</title>
    </head>

    <body style="background-color: #F9F9F9">

        <!-- Image Background Page Header -->
        <!-- Note: The background image is set within the business-casual.css file. -->
        <header class="business-header">
            <!-- Navigation -->
            <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/nav.php"); ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center h1-xl font-josefin text-white font-thick">CONTACT</h1>
                        <h3 class="text-center text-white font-thin">We Would Love TO Hear From You</h3>
                    </div>
                </div>
            </div>
        </header>

        <!-- Page Content -->
        <section class="container">
            <div class="row margin-top-xl margin-bottom-xl">
                <div class="col-sm-8">
                    <div class="row margin-bottom-lg">
                        <div class="col-xs-12">
                            <label>YOUR NAME (REQUIRED)</label>
                            <input type="text" name="name" id="name" class="form-control contact-input" />
                        </div>
                    </div>
                    <div class="row margin-bottom-lg">
                        <div class="col-xs-12">
                            <label>YOUR EMAIL (REQUIRED)</label>
                            <input type="text" name="email" id="email" class="form-control contact-input" />
                        </div>
                    </div>
                    <div class="row margin-bottom-lg">
                        <div class="col-xs-12">
                            <label>SUBJECT</label>
                            <input type="text" name="subject" id="subject" class="form-control contact-input" />
                        </div>
                    </div>
                    <div class="row margin-bottom-lg">
                        <div class="col-xs-12">
                            <label>YOUR MESSAGE (REQUIRED)</label>
                            <textarea type="text" name="msg" id="msg" class="form-control contact-input" ></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <button typ="button" id="submit_msg" class="btn btn-block background-green"><h4 class="font-open-sans font-thick text-white">SEND</h4></button>
                        </div>
                        <div class="col-xs-9" id="error-buffer"><!-- buffer --></div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h2 class="font-josefin font-thick text-blue margin-top-none">LET'S TALK</h2>
                    <p class="contact-p">We would love to hear from you. Please reach out to us at anytime to inquire about how we can care for you or your loved one. Just fill out the form to the left or reach us at the contact information below.</p>
                    <h3 class="font-open-sans text-light-blue">Phone:</h3>
                    <p class="contact-p">(209)728-7213</p>
                    <h3 class="font-open-sans text-light-blue">Email:</h3>
                    <p class="contact-p">info@serenityhome.care</p>
                    <h3 class="font-open-sans text-light-blue">Address:</h3>
                    <p class="contact-p">P.O. Box 731</p>
                    <p class="contact-p">Jamestown, CA 95327</p>
                </div>
            </div>

        </section><!-- /.container -->

        <section id="contact-map" ><!-- google map --></section>

        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/footer.html"); ?>

        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>

        <script type="text/javascript">
            jQuery(function ($) {
                function init_map() {
                    var myLocation = new google.maps.LatLng(37.954809, -120.422047);
                    var mapOptions = {
                        center: myLocation,
                        zoom: 18
                    };
                    var marker = new google.maps.Marker({
                        position: myLocation,
                        title: "Serenity Home Care PO Box 731 Jamestown, Ca 95327"
                    });
                    var map = new google.maps.Map(document.getElementById("contact-map"),
                            mapOptions);
                    marker.setMap(map);
                }
                init_map();
            });

            var $ = jQuery;
            $(document).ready(function () {
                $("#submit_msg").click(function () {
                    var name = $("#name").val();
                    var email = $("#email").val();
                    var message = $("#msg").val();
                    var subject = $("#subject").val();

                    // if requireds are filled then send email
                    if (name.length && email.length && message.length) {
                        $.ajax({
                            url: '/send_mail.php',
                            type: 'post',
                            data: {
                                name: name,
                                email: email,
                                message: message,
                                subject: subject
                            }
                        }).complete(function () {
                            $("#name").val("");
                            $("#email").val("");
                            $("#msg").val("");
                            $("#subject").val("");
                            // display success message
                            var x = $("#error-buffer");
                            x.empty(); // remove old messages
                            x.hide();
                            x.append("<p class='text-success text-center font-thick text-lg font-open-sans margin-top-sm'>Your message has been submitted, thank you!</p>");
                            x.fadeIn(1000);
                            x.delay(2000);
                            x.fadeOut(1000);

                        });
                    } else {
                        // display error message
                        var x = $("#error-buffer");
                        x.empty(); // remove old messages
                        x.hide();
                        x.append("<p class='text-danger text-center font-thick text-lg font-open-sans margin-top-sm'>Please fill out all required forms.</p>");
                        x.fadeIn(1000);
                        x.delay(1000);
                        x.fadeOut(1000);
                    }




                });
            });
        </script>



    </body>

</html>
