<html lang="en">
    <head>

        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/head.php"); ?>
        <!-- css is native to home page -->
        <link href="/css/carousel.css" rel="stylesheet">
        
        <!-- custom css to change menu attributes for index page only -->
        <style>
            #nav-logo {
                content:url("/imgs/nav-logo-blue.png");
            }
            .nav-item {
                color:#6098c3 !important;
            }
        </style>
        
    </head>
    <body cz-shortcut-listen="true">
        
        
        <!-- Full Page Image Background Carousel Header -->
        <header id="myCarousel" class="carousel slide">
            <!-- Navigation -->
            <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/nav.php"); ?>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('/imgs/home-helping-hand.jpg');">
                        <div class="caption row vertical-align">
                            <div class="col-md-2"><!-- buffer --></div>
                            <div class="col-md-10">
                                <h1 class="h1-xl text-blue font-thin">Serving You <br> With Dignity</h1>
                                <h3 class="text-blue">Helping You Live and Thrive at Home</h3>
                                <button class="btn background-blue margin-top-sm"><h5 class="text-bold text-white margin-sm">FIND OUT HOW &nbsp;<i class="fa fa-chevron-right"></i></h5></button>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-caption"></div>
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('/imgs/home-personal-touch.jpg');">
                        <div class="caption row vertical-align">
                            <div class="col-md-7"><!-- buffer for alignment--></div>
                            <div class="col-md-5">
                                <h1 class="h1-xl text-gold font-thin">Personal Touch</h1>
                                <div class="row">
                                    <h3 class="col-xs-10 text-blue text-justify margin-top-sm margin-bottom-sm">We believe quality interactions are key in the prosperity of your life. All our caregivers are bonded, reliable and bring their love and compassion in every visit.</h3>
                                    <div class="col-xs-2"><!-- buffer --></div>
                                </div>
                                <button class="btn background-gold margin-top-sm"><h5 class="text-bold text-white margin-sm">LEARN MORE &nbsp;<i class="fa fa-chevron-right"></i></h5></button>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('/imgs/home-lovely-lady.jpg');">
                        <div class="caption row vertical-align">
                            <div class="col-md-2"><!-- buffer for alignment--></div>
                            <div class="col-md-10">
                                <h1 class="h1-xl text-white font-thin">Flexible &amp;<br>Convenient</h1>
                                <div class="row">
                                    <h3 class=" col-xs-4 text-justify margin-top-sm margin-bottom-sm text-white">Our services extend outside the home with care for individuals within long-term and assisted living facilities.</h3>
                                    <div class="col-xs-8"><!-- buffer --></div>
                                </div>
                                <button class="btn background-green margin-top-sm"><h5 class="text-bold text-white margin-sm">MORE SERVICES &nbsp;<i class="fa fa-chevron-right"></i></h5></button>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('/imgs/home-nice-man.jpg');">
                        <div class="caption row vertical-align">
                            <div class="col-md-6"><!-- buffer for alignment--></div>
                            <div class="col-md-6">
                                <h1 class="h1-xl text-white font-thin">Piece of Mind</h1>
                                <div class="row">
                                    <h3 class="col-xs-8 text-white text-justify margin-top-sm margin-bottom-sm">You will be at ease knowing you are loved, taken care of and treated like family. We go above and beyond to ensure you experience a greater level of success in your home.</h3>
                                    <div class="col-xs-4"><!-- buffer --></div>
                                </div>
                                <button class="btn background-gold margin-top-sm"><h5 class="text-bold text-white margin-sm">LEARN MORE &nbsp;<i class="fa fa-chevron-right"></i></h5></button>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('/imgs/home-connect.jpg');">
                        <div class="caption row vertical-align">
                            <div class="col-lg-12">
                                <h1 class="h1-xl font-thin text-white text-center">Let's Connect</h1>
                                <h1 class="h2-lg h1-xs text-white text-center font-thick">START YOUR CARE WITH US TODAY!</h1>
                                <h1 class="h2-lg h1-xs text-white text-center font-thick">CALL: (209) 728-7213</h1>
                                <div class="row margin-top-lg">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4 text-center">
                                        <button class="btn background-gold"><h5 class="text-bold text-white margin-sm">CONTACT US &nbsp;<i class="fa fa-chevron-right"></i></h5></button>
                                    </div>
                                    <div class="col-sm-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"><i class="fa fa-chevron-left"></i></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"><i class="fa fa-chevron-right"></i></span>
            </a>

        </header>

        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/footer.html"); ?>

        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 5000 //changes the speed
            })
        </script>




    </body>
</html>