<!-- Navigation -->
<script>
    var init_nav_active = true;

    function useScrolledNav() {
        $('nav').hide().removeClass('navbar-static-top').addClass('navbar-fixed-top').fadeIn(250);
        $('#nav-logo').attr('src', '/imgs/nav-logo-blue.png');
        init_nav_active = false;
    }

    function useInitNav() {
        $('nav').hide().removeClass('navbar-fixed-top').addClass('navbar-static-top').fadeIn(250);
        $('#nav-logo').attr('src', '/imgs/nav-logo-white.png');
        init_nav_active = true;
    }


    $(function() {
        if ($(window).scrollTop() > 300) {
            useScrolledNav();
        }

        $(window).scroll(function () {
            if ($(this).scrollTop() > 300 && init_nav_active) {
                useScrolledNav();
            } else if ($(window).scrollTop() < 300 && !init_nav_active) {
                useInitNav();
            }
        });
    });
    
    $(function(){
       $( "#nav-toggle-button" ).click(function() {
            var sign = "+";
            if ($(".navbar").first().height() > 250) {
                sign = "-";
            }
        
            $( ".navbar" ).animate({
                height: sign + "=200",
            }, 250, function() {
                // Animation complete.
            });
        });
    });
</script>
<!-- Navigation -->
<nav class="navbar navbar-static-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" id="nav-toggle-button" data-target="#nav-toggler">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand pull-left" href="/index.php"><img id="nav-logo" src="/imgs/nav-logo-white.png" /></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse float-right" id="nav-toggler">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php"><p class="nav-item">HOME</p></a>
                </li>
                <li>
                    <a href="about.php"><p class="nav-item">ABOUT</p></a>
                </li>
                <li>
                    <a href="contact.php"><p class="nav-item">CONTACT</p></a>
                </li>
                <li>
                    <a href="jobs.php"><p class="nav-item">JOBS</p></a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
</nav>
