<?php

/*
  The about page for SHC

  Created on : Feb 16, 2016, 11:07:21 AM
  Author     : Dominic Dambrogia
  Contact    : domdambrogia@gmail.com
 */

?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/head.php"); ?>
        <title>About | Serenity Home Care</title>
        <link href="/css/slippry.css" rel="stylesheet" />
        <script src="/js/slippry.js"></script>
    </head>

    <body style="background-color: #F9F9F9">



        <!-- Image Background Page Header -->
        <!-- Note: The background image is set within the business-casual.css file. -->
        <header class="business-header">
            <!-- Navigation -->
            <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/nav.php"); ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center h1-xl font-josefin text-white font-thick">ABOUT US</h1>
                        <h3 class="text-center text-white font-thin">We Deliver Premier In-Home Care</h3>
                    </div>
                </div>
            </div>
        </header>

        <!-- Page Content -->
        <div class="container margin-bottom-xl">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-center h1-xs font-josefin text-light-blue font-thick margin-top-xl">CARE THAT COUNTS</h1>
                    <h3 class="text-blue text-center font-open-sans margin-top-lg">Serenity Home Care is a premier in-home care service with a reputation built on relationships. Our focus is your well being, safety, comfort and growth. We deliver on these core values by creating a custom care management plan and adding in our personal touch.</h3>
                    <h1 class="text-center h1-xs font-josefin text-light-blue font-thick margin-top-xl">WHAT CAN WE HELP YOU WITH?</h1>
                    <div class="row padding-left-md margin-top-xl">
                        <div class="col-sm-4">
                            <h3 class="text-green font-josefin font-thick margin-bottom-md"><i class="fa fa-lg fa-user"></i> &nbsp;PERSONAL CARE</h3>
                            <ul class="personal_care_ul">
                                <li><p class="help_p">Companionship</p></li>
                                <li><p class="help_p">Disability</p></li>
                                <li><p class="help_p">Client Advocacy</p></li>
                                <li><p class="help_p">Respite Care</p></li>
                                <li><p class="help_p">Medicine Management</p></li>
                                <li><p class="help_p">Hospice Care</p></li>
                                <li><p class="help_p">Short-Term Assistance</p></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="text-green font-josefin font-thick margin-bottom-md"><i class="fa fa-lg fa-home"></i> &nbsp;DOMESTIC</h3>
                            <ul class="personal_care_ul">
                                <li><p class="help_p">Light Household Cleaning</p></li>
                                <li><p class="help_p">Laundry / Washing</p></li>
                                <li><p class="help_p">Meal Preparation</p></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="text-green font-josefin font-thick margin-bottom-md"><i class="fa fa-lg fa-car"></i> &nbsp;TRANSPORTAION</h3>
                            <ul class="personal_care_ul">
                                <li><p class="help_p">Doctor Appointments</p></li>
                                <li><p class="help_p">Pharmacy Visits</p></li>
                                <li><p class="help_p">Errands Service</p></li>
                                <li><p class="help_p">Errands Service</p></li>
                                <li><p class="help_p">Grocery Shopping</p></li>
                                <li><p class="help_p">Recreation Activities</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
        <!-- @TODO make mobile css rule for this section -->
        <div id="testimonials">
            <div class="row" style="transform: translateY(30%);">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <ul id="slider" class="col-md-8">
                        <li>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <h1 class="text-center text-white h1-xs font-open-sans font-thick margin-bottom-md">JOHN COSTA</h1>
                                    <p class="testimonial-quote">My father needed care that the family could not provide. Luckily we were referred to Tammy Ennis, Owner of Serenity Home Care. She was able to coordinate the care my father needed with very competent, honest and professional help. Reasonably priced and reliable. I would recommend anyone needing this kind of service to contact tammy and give her an opportunity to help your family as she helped ours. I will always be grateful for her honesty, integrity and service provided.</p>                        
                                </div>
                                <div class="col-sm-2"></div>
                            </div>                                
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <h1  class="text-center text-white h1-xs font-open-sans font-thick margin-bottom-md">PHYLLIS FERREL-ANDERSON</h1>
                                    <p class="testimonial-quote">Tammy Ennis and Serenity Home Care have been a huge blessing in mine and my mother's lives. Five years ago I needed help caring for my mother who had been diagnosed with Alzheimer's disease. I wanted someone who would treat my mother with the respect and care she deserved, with Serenity Home Care I found that and more. Tammy cared for my mother as if she was her own mother and I often told people, "Tammy is the second daughter that my mother never had". I highly recommend Serenity Home Care for anyone who is in need of help for themselves or a loved one.</p>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <h1  class="text-center text-white h1-xs font-open-sans font-thick margin-bottom-md">MARK E OLSON</h1>
                                    <p class="testimonial-quote">I have used Tammy Ennis's Serenity Home Care to take care of a client of mine who is an 84 year old man with dementia. Her staff is professional and care of him like he is family rather than just a job. As a conservator for multiple people I have seen the gammut of home care. Serenity Home Care offers a quality service at an affordable price.</p>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <div>
            <div class="row margin-top-xl">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4"><img style="max-width:90%; display:block; margin:0 auto;"src="/imgs/tammy-ennis.png" /></div>
                        <div class="col-sm-4"></div>
                    </div>
                    <div class="row margin-top-md margin-bottom-xl">
                        <div class="col-lg-12">
                            <h2 class="text-center font-josefin font-thick text-light-blue">“WE ARE HONORED AND EXCITED TO WORK WITH YOU AND WELCOME YOU TO THE SERENITY FAMILY.”</h2>
                            <h3 class="text-center font-open-sans margin-top-md" style="color:#565656;"> ~ TAMMY ENNIS, OWNER</h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <div class="background-blue" style="height:150px;">
            <div class="container vertical-align" style="margin:0 auto;">
                <p class="float-left text-white margin-top-sm text-lg">Like what you see? Click here to contact us about how to start your care.</p>
                <div class="float-right"><a href="/contact.php"><button class="btn background-white float-right"><h4 class="text-blue font-open-sans margin-xs">CLICK HERE</h4></button></a></div>
            </div>
        </div>

        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/footer.html"); ?>




        <!-- Bootstrap Core JavaScript -->
        <script src="/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                jQuery(document).ready(function () {
                    jQuery('#slider').slippry();
                });
            });
        </script>

    </body>

</html>
